import pygame
from player import Player

pygame.init() # initialiser le module pygame

# 'display' définit des paramètres d'affichage de la fenêtre.
# définit le titre de la fenêtre
pygame.display.set_caption("Mon Jeu à moi..")
WIDTH = 1500
HEIGHT = 600
# génère la fenêtre, et la taille de la fenêtre
# mais la fenêtre se referme instantanément
screen = pygame.display.set_mode((WIDTH, HEIGHT)) # renvoie une surface

# En 3 étapes
# Etape 1: Importe une image dans pygame
background = pygame.image.load('assets/bg.jpg')

# créer une instance du joueur
player = Player()

# Pour maintenir la fenêtre affichée, écouter les événements
running = True
while running:
  # Etape 2: Applique la fenêtre/surface à une position spécifique d'une image
  screen.blit(background, (0,-400))

  # Applique l'image du joueur à une position spécifique de l'écran
  # screen.blit(player.image, (0,0))
  screen.blit(player.image, player.rect)

  # Etape 3: mettre à jour l'écran
  pygame.display.flip()

  # event.get() renvoie une liste des événements
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
      pygame.quit()
      print("Fermeture du Script")
