import pygame
class Player(pygame.sprite.Sprite):
  def __init__(self):
    super().__init__()
    # caractéristiques par défaut du joueur
    self.health = 100
    self.max_health = 100
    self.attack = 10
    self.velocity = 5 # en pixels
    self.image = pygame.image.load('assets/player.png')
    self.rect = self.image.get_rect() ## coordonnées de l'image
    self.rect.x = 640
    self.rect.y = 300
