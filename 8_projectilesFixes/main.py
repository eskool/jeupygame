import pygame
# from player import Player
from game import Game

pygame.init() # initialiser le module pygame

# 'display' définit des paramètres d'affichage de la fenêtre.
# définit le titre de la fenêtre
pygame.display.set_caption("Mon Jeu à moi..")
WIDTH = 1500
HEIGHT = 720
# génère la fenêtre, et la taille de la fenêtre
# mais la fenêtre se referme instantanément
screen = pygame.display.set_mode((WIDTH, HEIGHT)) # renvoie une surface

# En 3 étapes
# Etape 1: Importe une image dans pygame
background = pygame.image.load('assets/bg.jpg')

# créer une instance du joueur
# player = Player()
# ou mieux : charger une instance du Jeu
game = Game()

# Pour maintenir la fenêtre affichée, écouter les événements
running = True
while running:
  # Etape 2: Applique la fenêtre/surface à une position spécifique d'une image
  screen.blit(background, (0,-280))

  # Applique l'image du joueur à une position spécifique de l'écran
  # screen.blit(player.image, (0,0))
  screen.blit(game.player.image, game.player.rect)

  # Appliquer l'ensemble des images du groupe de projectiles sur la fenêtre/surface
  game.player.all_projectiles.draw(screen)

  # vérifier si le joueur souhaite aller à gauche ou à droite
  if game.pressed.get(pygame.K_RIGHT) and game.player.rect.x + game.player.rect.width <= screen.get_width(): # vérifie si la touche DROITE est active
    game.player.move_right()
  if game.pressed.get(pygame.K_LEFT) and game.player.rect.x >= 0: # vérifie si la touche DROITE est active
    game.player.move_left()

  # Etape 3: mettre à jour l'écran
  pygame.display.flip()

  # event.get() renvoie une liste des événements
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
      pygame.quit()
      print("Fermeture du Script")
    # détecte si un joueur lâche une touche du clavier
    elif event.type == pygame.KEYDOWN:
      game.pressed[event.key] = True
      
      # détecter si la touche est ESPACE
      if event.key == pygame.K_SPACE:
        game.player.launch_projectile()


    elif event.type == pygame.KEYUP:
      game.pressed[event.key] = False


