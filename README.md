# Jeu Pygame

Créer un jeu en Pygame

## Créer une fenêtre Graphique

* `pygame.display.set_caption()` personnalise le titre de la fenêtre graphique
* `pygame.display.set_mode()` génère la fenêtre graphique, mais ne la maintient pas ouverte, car aucun événement n'est monitoré/surveillé. En fait `set_mode()` envoie une `surface`
* `pygame.event.get()` renvoie une liste d'événements:
* `event.type`: renvoie le type d'événement
* `pygame.quit()` ferme la fenêtre graphique

## Ajouter un arrière plan

`screen = pygame.display.set_mode((WIDTH, HEIGHT))` renvoie une surface

L'insertion d'une image en arrière-plan se fait en 3 étapes:

* Etape 1: Importe une image dans pygame
`background = pygame.image.load('assets/bg.jpg')`

* Etape 2: Applique une image à endroit spécifique de la fenêtre/surface
`screen.blit(background, (0,0))`

* Etape 3: mettre à jour l'écran
`pygame.display.flip()`

## Joueurs

### Un joueur = une Classe

Une bonne pratique consiste à créer une classe pour chaque composant du jeu:

* une classe pour le joueur
* une classe pour le monstre
* une classe pour une comète, etc..
* une classe pour le Jeu

#### Définir une classe comme étant un sprite

Lorsqu'une classe doit être considérée comme une composante graphique du jeu, il faudra la faire hériter de la super classe `Sprite` qui est la classe de base de tous les éléments visibles du jeu. On disposera alors de :

* `Sprite.update()` pour mettre à jour le sprite
* `Sprite.image()` pour mettre à jour l'image
* `Sprite.rect()` pour mettre à jour le rectangle

#### Définir une classe pour le Jeu

### Déplacements du Joueur

#### Déplacement par appui Simple de Touche

* Dans la classe `Player`, ajouter une méthode `mode_right(self)`, et incrémenter `self.rect.x` de `self.velocity`
* Dans la classe `Player`, ajouter une méthode `mode_left(self)`, et décrémenter `self.rect.x` de `self.velocity`
* `event.key` fait référence à la touche enfoncée
* `pygame.K_RIGHT` fait référence à la touche droite, etc..

```python linenums="0"
if event.type == pygame.KEYDOWN:
  if event.key == pygame.K_RIGHT:
    player.move_right()
```

#### Déplacements avec Répétition par touche appuyée

* Dans la classe `Game` créer un dictionnaire `self.pressed`, qui 
    * est initialisé à vide `{}`
    * contiendra les touches (déjà) appuyées, et leur état booléen
* Dans `main.py`, on aura accès au dictionnaire des touchés pressées `game.pressed`

```python linenums="0"
if event.type == pygame.KEYDOWN:
    game.pressed[event.key] = True
elif event.type == pygame.KEYUP:
  game.pressed[event.key] = False
```

### Sorties Latérales

#### Empêcher de sortir

* position du joueur en x doit être positive
* position du joueur en x doit être inférieure à la largeur de la fenêtre

#### Faire un Cycle

Càd revenir de l'autre côté

## Projectiles

### Afficher un Projectile en position absolue

* Créer une classe `Projectile` ayant les attributs suivants
    * `self.velocity`
    * `self.image` qui charge une image dans pygame
* Redimensionner l'image du projectile avec `pygame.transform.scale(self.image, (50, 50))` en `50px fois 50px`
* Dans la classe `Player`, Créer une méthode `launch_projectile(self)` en charge de:
    * Créer une instance de la classe Projectile (importer la classe Projectile avant tout)
    * afficher le projectile, qui est appelée lorsque la touche `SPACE` est appuyée

### Ajouter tous les projectiles dans un groupe

La logique précédente n'est pas adaptée à la création de nombreux projectiles, ce qui est souhaitable dans un tel jeu. On revoit donc la logique pour regrouper tous les projectiles dans un même **Groupe** :

* Dans la classe Player, ajouter un attribut `all_projectiles` par `self.all_projectiles = pygame.sprite.Group()` qui contiendra le **Groupe** de tous les sprites de projectiles
* la méthode `launch_projectile(self)` devra alors être adaptée pour qu'elle ajoute ce projectile dans le groupe `self.all_projectiles` grâce à une méthode `add()`:
`self.all_projectiles.add(Projectile())`

### Afficher un Projectile en position relative au joueur

* Dans la classe Player: modifier la méthode lauchn_projectile pour qu'il transmette le player (`self`) lors d'un ajout de nouveau projectile: `self.all_projectiles.add(Projectile(self))`
* Dans la classe Projectile:
    * Adpater son constructeur `__init__(self, player)` pour qu'il récupère le `player`
    * Positionner le projectile relativement à la position du joueur grâce à:
        * ``self.rect.x = player.rect.x``
        * ``self.rect.y = player.rect.y``
* Dans `main.py`, Afficher tous les projectiles du groupe self.all_projectiles grâce à: `game.player.all_projectiles.draw(screen)`

### Animer Linéairement les projectiles

* Dans la classe Projectile, Créer une méthode `move(self)` qui incrémente sa position horizontale `self.rect.x` de sa valeur de vitesse `self.velocity`
* Dans `main.py`, mais **avant** l'affichage du groupe de projectiles, parcourir tous les projectiles du groupe `game.player.all_projectiles` et déplacer chacun d'entre eux grâce à `projectile.move()`
* Dans la classe Projectile, Créer une méthode `remove(self)` qui supprimera ce projectile du groupe `player.all_projectiles`
* Dans la classe projectile, Modifier la méthode `move(self)` pour qu'elle vérifie si le projectile est hors écran, auquel cas il faudra le supprimier du groupe des projectiles `all_projectiles` avec la méthode `remove()` précédente

### Rotation du projectile en mouvement

* Dans la classe Projectile, ajouter deux attributs :
    * `self.origin_image` qui stockera la valeur originale de l'image
    * `self.angle` qui stockera l'angle de rotation, initialisé à `0`
*  Créer une méthode `rotate(self)` qui:
    * Incrémente l'angle de rotation de 12 degrés
    * Crée une rotation de l'image originale self.`origin_image` d'une valeur de self.`angle`, grâce à la syntaxe: `self.image = pygame.transform.rotozoom(self.origin_image, self.angle, 1)` (le `1` est un facteur de zoom)
    * Modifier le centre de rotation (par défaut, c'est le coin supérieur haut du projectile) de sorte que celui-ci soit le centre du projectile, grâce à la syntaxe: `self.rect = self.image.get_rect(center=self.rect.center)`

## Monstres

### Classe Monster

* Créer une classe Monster qu ihérite de la super classe `pygame.sprite.Sprite` qui:
    * admet un contructeur `__init__(self)` qui:
        * hérite de sa classe mère `Sprite`, grâce à `super()`
        * définit les attributs:
            * `self.health = 100`
            * `self.max_health = 100`
            * `self.attack = 5`
        * charge l'image du montre dans `self.image`
        * récupère ses coordonnées dans `self.rect`

### Dans la classe Game:

* Importer la classe `Monster`
* Modifier le constructeur `__init__()` pour que celui-ci définisse un nouvel attribut `self.all_monsters` contenant un groupe de sprites vide, grâce à la syntaxe : `self.all_monsters = pygame.sprite.Group()`
* créer une méthode `span_monster(self)` qui ajoute un nouveau monstre à **faire apparaître** :fr: (<bred>spawner</bred> :gb:) dans le groupe `self.all_monsters`

### Dans `main.py`, après l'apparition des projectiles:

* Appliquer l'ensemble des images du groupe de Monstre

### Revenir dans la Classe Monster

* Repositionner correctement le monstre, en ajouter/adaptant les attributs suivants:
    * `self.rect.x = 1000`
    * `self.rect.x = 460`
    * `self.velocity = 5`