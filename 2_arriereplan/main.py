import pygame
pygame.init() # initialiser le module pygame

# 'display' définit des paramètres d'affichage de la fenêtre.
# définit le titre de la fenêtre
pygame.display.set_caption("Mon Jeu à moi..")
WIDTH = 1920
HEIGHT = 1080
# génère la fenêtre, et la taille de la fenêtre
# mais la fenêtre se referme instantanément
screen = pygame.display.set_mode((WIDTH, HEIGHT)) # renvoie une surface

# En 3 étapes
# Etape 1: Importe une image dans pygame
background = pygame.image.load('assets/bg.jpg')

# Pour maintenir la fenêtre affichée, écouter les événements
running = True
while running:
  # Etape 2: Applique une image à endroit spécifique de la fenêtre/surface
  screen.blit(background, (0,0))

  # Etape 3: mettre à jour l'écran
  pygame.display.flip()

  # event.get() renvoie une liste des événements
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
      pygame.quit()
      print("Fermeture du Script")
