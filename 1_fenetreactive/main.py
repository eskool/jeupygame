import pygame
pygame.init() # initialiser le module pygame

# 'display' définit des paramètres d'affichage de la fenêtre.
# définit le titre de la fenêtre
pygame.display.set_caption("Mon Jeu à moi..")
WIDTH = 600
HEIGHT = 400
# génère la fenêtre, et la taille de la fenêtre
# mais la fenêtre se referme instantanément
pygame.display.set_mode((WIDTH, HEIGHT))

# Pour maintenir la fenêtre affichée, écouter les événements
running = True
while running:
  # Parcourir la liste des événements
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
      pygame.quit()
      print("Fermeture du Script")
