import pygame
from projectile import Projectile

class Player(pygame.sprite.Sprite):
  def __init__(self):
    super().__init__()
    # caractéristiques par défaut du joueur
    self.health = 100
    self.max_health = 100
    self.attack = 10
    self.velocity = 5 # en pixels
    self.all_projectiles = pygame.sprite.Group() #
    self.image = pygame.image.load('assets/player.png')
    self.rect = self.image.get_rect() ## coordonnées de l'image
    self.rect.x = 640
    self.rect.y = 420
  
  def launch_projectile(self):
    # Créer une instance de la classe projectile
    # projectile = Projectile()
    self.all_projectiles.add(Projectile(self))

  def move_right(self):
    self.rect.x += self.velocity

  def move_left(self):
    self.rect.x -= self.velocity
  
